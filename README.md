# `Help Desk` � Cryo

##Objetivo
Realizar un sistema web generador de tickets para la atencion oportuna de los usuarios.

A funcion de la mesa de ayuda es proveer a los usuarios un punyo �nico de contacto mediante
el cual se resuelvan y/o canalicen sus necesidades relativas al uso de recuros y servicios
de plataformas tecnol�gicas, siempre de acuerdo a un estandar adoptado por la empresa.

##Dentro de los objetivos de una mesa de ayuda se encuentran:

* Atender todas las llamadas recibidas.
* Resolver un alto porcentaje en l�nea.
* Seguimiento en l�nea de los casos derivados.
* Reducir llamados recurrentes en el tiempo.



### Prerequisites

* Descargar repositorio de  `git@gitlab.com:AlejandroBec94/sservice.git` repository.
* Instalaci�n de NodeJs

### Clone `sservice`

Clone the `sservice` repository using git:

```
git clone git@gitlab.com:AlejandroBec94/sservice.git
cd sservice

```

### Install Dependencies

Actualizaci�n de Dependencias en caso de ser necesario.

```
npm install

```

*Nota: No es recomendado, ya que si se utiliza una dependencia en espec�fico, puede afectar a la arquitectura.*

###Correr la App

Para iniciar el servidor:

```
npm start
```

Now browse to the app at [`localhost:8000/index.html`][local-app-url].


## Directorio

```
app/                    --> all of the source files for the application
  app.css               --> default stylesheet
  components/           --> all app specific modules
    version/              --> version related components
      version.js                 --> version module declaration and basic "version" value service
      version_test.js            --> "version" value service tests
      version-directive.js       --> custom directive that returns the current app version
      version-directive_test.js  --> version directive tests
      interpolate-filter.js      --> custom interpolation filter
      interpolate-filter_test.js --> interpolate filter tests
  view1/                --> the view1 view template and logic
    view1.html            --> the partial template
    view1.js              --> the controller logic
    view1_test.js         --> tests of the controller
  view2/                --> the view2 view template and logic
    view2.html            --> the partial template
    view2.js              --> the controller logic
    view2_test.js         --> tests of the controller
  app.js                --> main application module
  index.html            --> app layout file (the main html template file of the app)
  index-async.html      --> just like index.html, but loads js files asynchronously
karma.conf.js         --> config file for running unit tests with Karma
e2e-tests/            --> end-to-end tests
  protractor-conf.js    --> Protractor config file
  scenarios.js          --> end-to-end scenarios to be run by Protractor
```


### Running the App during Development

The `angular-seed` project comes preconfigured with a local development web server. It is a Node.js
tool called [http-server][http-server]. You can start this web server with `npm start`, but you may
choose to install the tool globally:

```
sudo npm install -g http-server
```

Then you can start your own development web server to serve static files from a folder by running:

```
http-server -a localhost -p 8000
```

###Para contribuir con el proyecto

### Alejandro Becerra Ortiz

* Repositorio :[SService][Bec-git-lab]
* Contacto : `a.becor94@gmail.com` 
* Linkedin : [https://www.linkedin.com/in/alejandro-becerra-ortiz/][https://www.linkedin.com/in/alejandro-becerra-ortiz/]



[Bec-git-lab]: https://gitlab.com/AlejandroBec94/sservice
[https://www.linkedin.com/in/alejandro-becerra-ortiz/]: https://www.linkedin.com/in/alejandro-becerra-ortiz/

